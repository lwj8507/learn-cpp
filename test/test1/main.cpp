#include <iostream>
#include <cstdint>
#include <random>
using namespace std;

bool IntervalUpdate(int32_t &alive, int32_t interval, int32_t escaple, int32_t &update)
{
    alive -= escaple;
    update += escaple;
    // if (update == escaple)
    //{
    //     return true;
    // }
    if (update < interval)
    {
        return false;
    }
    update -= interval;
    return true;
}

int main(int argc, char *argv[])
{
    int32_t alive = 8000;
    int32_t interval = 1000;
    int32_t escaple = 100;
    int32_t update = 0;
    int32_t count = 0;

    std::random_device rd;
    std::mt19937_64 engine(rd());
    std::uniform_int_distribution<> dice(90, 110);

    while (alive > 0)
    {
        escaple = dice(engine);
        if (IntervalUpdate(alive, interval, escaple, update))
        {
            count++;
            cout << "escaple:" << escaple << ", alive:" << alive << ", count:" << count << endl;
        }
    }

    // cout << count << endl;

    return 0;
}
