#include <iostream>

using namespace std;

struct obj
{
	int n;

	obj()
	{
		cout << "obj(), this=" << this << endl;
	}

	explicit obj(int n)
		: n(n)
	{
		cout << "obj(int n), this=" << this << ", n=" << n << endl;
	}

	obj(const obj &other)
	{
		cout << "obj(const obj& other), this=" << this << ", other=" << &other << endl;
		this->n = other.n;
	}

	~obj()
	{
		cout << "~obj(), this=" << this << endl;
	}

	obj &operator=(const obj &other)
	{
		cout << "operator=(), this=" << this << ", other=" << &other << endl;
		this->n = other.n;
		return *this;
	}
};

obj foo()
{
	obj o(1);
	return o;
}

obj bar()
{
	return obj(1);
}

int main(int argc, char **argv)
{
	obj obj1 = foo();
	cout << "&obj1=" << &obj1 << endl;

	obj obj2 = bar();
	cout << "&obj2=" << &obj2 << endl;
	return 0;
}
