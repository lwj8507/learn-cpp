﻿#include <iostream>
#include <cstdlib>
#include <cstdint>
using namespace std;

struct VendingInfo
{
    int64_t BeginSellTime = 0; // 开始贩卖的时间

    enum EField
    {
        F_BeginSellTime,
    };

    template <EField field>
    const auto &Get() const
    {
        if constexpr (field == F_BeginSellTime)
        {
            return BeginSellTime;
        }
        else
        {
            static_assert(true, "unsupported VendingInfo field");
        }
    }

    template <EField field, typename Value>
    void Set(const Value &value)
    {
        if constexpr (field == F_BeginSellTime)
        {
            BeginSellTime = value;
        }
        else if constexpr (std::is_array<Value>::type)
        {
        }
        else
        {
            static_assert(true, "unsupported VendingInfo field");
        }
    }
};

int main(int argc, char *argv[])
{
    VendingInfo i;
    i.Set<VendingInfo::F_BeginSellTime>(1);
    cout << i.Get<VendingInfo::F_BeginSellTime>() << endl;
    return 0;
}
