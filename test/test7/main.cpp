﻿#include <iostream>
#include <vector>
#include <cstdint>
#include <string>
using namespace std;

struct employee
{
    uint32_t id;
    std::string name;
    std::string role;
    uint32_t salary;
};

int main()
{
    employee e{1, "e", "r", 1000};
    auto &[_, name, role, salary] = e;
    cout //<< "ID: " << id
        << " Name: " << name
        << " Role: " << role
        << " Salary: " << salary << endl;

    std::vector<employee> employees{e};
    for (const auto &[_, name, role, salary] : employees)
    {
        cout //<< "ID: " << id
            << " Name: " << name
            << " Role: " << role
            << " Salary: " << salary << endl;
    }
}
