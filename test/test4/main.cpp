﻿#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstdint>
#include <unordered_map>
using namespace std;

enum Offset
{
    O_1 = 1,
    O_2,
    O_3,
    O_4,
};

enum Enum
{
    E_1 = 1 << O_1,
    E_2 = 1 << O_2,
    E_3 = 1 << O_3,
    E_4 = 1 << O_4,
};

#define HAS_TAG(sign, tag) (((sign) & (tag)) != 0)

int main(int argc, char *argv[])
{
    // std::srand(uint32_t(std::time(nullptr)));

    // int max = 0;
    // int r = 0;
    // std::unordered_map<int, int> s;
    // for (int i = 0; i < 1000; ++i)
    //{
    //     r = std::rand() % 6;
    //     if (r > max)
    //     {
    //         max = r;
    //     }
    //     s[r]++;
    // }

    // cout << "max: " << max << endl;

    // for (const auto& itr : s)
    //{
    //     cout << itr.first << ": " << itr.second << endl;
    // }

    int32_t sign = E_1 | E_2 | E_3 | E_4;

    cout << sign << endl;

    cout << HAS_TAG(sign, E_1) << endl;
    cout << HAS_TAG(sign, E_2) << endl;
    cout << HAS_TAG(sign, E_3) << endl;
    cout << HAS_TAG(sign, E_4) << endl;

    return 0;
}
