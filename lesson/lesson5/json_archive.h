#pragma once

#include <string>
#include <set>
#include <unordered_set>

#include "noncopyable.h"
#include "type_traits.hpp"
#include "string.hpp"

#include <boost/mpl/range_c.hpp>
#include <boost/fusion/include/for_each.hpp>
#include <boost/fusion/include/zip.hpp>
#include <boost/fusion/include/at_c.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/mpl.hpp>

#include <boost/optional.hpp>

#ifdef RAPIDJSON_HAS_STDSTRING
#undef RAPIDJSON_HAS_STDSTRING
#endif
#define RAPIDJSON_HAS_STDSTRING 1
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/document.h>

namespace bzbee
{
    namespace Serialization
    {

        // #ifndef NDEBUG
        // #define BOYEE_JSON_INPUT_ARCHIVER_USE_PRETTY_WRITER
        // #endif

        class JsonSerializer : NonCopyable
        {
        public:
            JsonSerializer()
                : writer(stringBuffer)
            {
            }

            ~JsonSerializer()
            {
            }

            template< typename T >
            void serialize(const T& t)
            {
                writer.Reset(stringBuffer);
                stringBuffer.Clear();
                (*this)(t);
            }

            const char* getString() const
            {
                return stringBuffer.GetString();
            }

            std::size_t getSize() const
            {
                return stringBuffer.GetSize();
            }

        private:
            template< typename T >
            typename std::enable_if< std::is_integral< decay_t<T> >::value
                && std::is_signed< decay_t<T> >::value
                && (sizeof(T) <= sizeof(int32_t))
                , void >::type
                operator () (const T& t) const
            {
                writer.Int(t);
            }

            template< typename T >
            typename std::enable_if< std::is_integral< decay_t<T> >::value
                && !std::is_signed< decay_t<T> >::value
                && (sizeof(T) <= sizeof(int32_t))
                , void >::type
                operator () (const T& t) const
            {
                writer.Uint(t);
            }

            template< typename T >
            typename std::enable_if< std::is_integral< decay_t<T> >::value
                && std::is_signed< decay_t<T> >::value
                && (sizeof(T) > sizeof(int32_t) && sizeof(T) <= sizeof(int64_t))
                , void >::type
                operator () (const T& t) const
            {
                writer.Int64(t);
            }

            void operator() (bool t) const
            {
                writer.Bool(t);
            }

            template< typename T >
            typename std::enable_if< std::is_integral< decay_t<T> >::value
                && !std::is_signed< decay_t<T> >::value
                && (sizeof(T) > sizeof(int32_t) && sizeof(T) <= sizeof(int64_t))
                , void >::type
                operator () (const T& t) const
            {
                writer.Uint64(t);
            }

            void operator & (bool t) const
            {
                writer.Bool(t);
            }

            template< typename T >
            typename std::enable_if< std::is_floating_point< decay_t<T> >::value, void >::type
                operator () (const T& t) const
            {
                writer.Double(t);
            }

            template< typename T >
            typename std::enable_if< isString< T >::value, void >::type
                operator () (const T& t) const
            {
                writer.String(t);
            }

            template< typename T >
            typename std::enable_if< boost::fusion::traits::is_sequence< T >::type::value
                && !isSingleValueContainer< T >::value
                && !isMapContainer< T >::value
                , void >::type
                operator () (const T& t) const
            {
                writer.StartObject();
                using Indices = boost::mpl::range_c< std::size_t, 0, boost::fusion::result_of::size< T >::value >;
                boost::fusion::for_each(Indices(), Visitor<T>(*this, t));
                writer.EndObject();
            }

            template< typename T >
            typename std::enable_if< isSingleValueContainer< T >::value
                , void >::type
                operator () (const T& t) const
            {
                writer.StartArray();
                for (const auto& value : t)
                {
                    (*this)(value);
                }
                writer.EndArray();
            }

            template< typename T >
            typename std::enable_if< isMapContainer< T >::value
                , void >::type
                operator () (const T& t) const
            {
                writer.StartObject();
                for (const auto& pair : t)
                {
                    writer.String(std::to_string(pair.first));
                    (*this)(pair.second);
                }
                writer.EndObject();
            }

            template < typename T, size_t N >
            void operator () (const T(&t)[N]) const
            {
                writer.StartArray();
                for (const auto& value : t)
                {
                    (*this)(value);
                }
                writer.EndArray();
            }

            template< typename T, size_t N >
            void operator () (const boost::array<T, N>& t) const
            {
                writer.StartArray();
                for (const auto& value : t)
                {
                    (*this)(value);
                }
                writer.EndArray();
            }

            template< typename T >
            void operator () (const char* szFieldName, const T& t) const
            {
                writer.String(szFieldName);
                (*this)(t);
            }

            template< typename T >
            void operator () (const char* szFieldName, const boost::optional< T >& t) const
            {
                if (t)
                {
                    writer.String(szFieldName);
                    (*this)(*t);
                }
            }

            template < typename Seq >
            struct Visitor : NonCopyable
            {
                const JsonSerializer& serializer;
                const Seq& seq;

                Visitor(const JsonSerializer& serializer_, const Seq& seq_)
                    : serializer(serializer_)
                    , seq(seq_)
                {
                }

                template< typename Index >
                void operator() (Index&& index) const
                {
                    const std::string& fieldName = boost::fusion::extension::struct_member_name< Seq, Index::value >::call();
                    serializer(fieldName.c_str(), boost::fusion::at< Index >(seq));
                }
            };

        private:
            mutable rapidjson::StringBuffer stringBuffer;

#ifdef BOYEE_JSON_INPUT_ARCHIVER_USE_PRETTY_WRITER
            mutable rapidjson::PrettyWriter< rapidjson::StringBuffer > writer;
#else
            mutable rapidjson::Writer< rapidjson::StringBuffer > writer;
#endif
        };

        class JsonDeserializer : NonCopyable
        {
        public:
            JsonDeserializer()
            {
            }

            ~JsonDeserializer()
            {
            }

            template< typename T >
            void deserialize(const char* json, T& t)
            {
                if (reader.Parse(json).HasParseError())
                {
                    throw std::invalid_argument("invalid json string");
                }

                (*this)(reader, t);
            }

        private:
            template< typename T >
            typename std::enable_if< std::is_integral< decay_t<T> >::value
                && std::is_signed< decay_t<T> >::value
                && (sizeof(T) <= sizeof(int32_t))
                , void >::type
                operator () (const rapidjson::Value& value, T& t) const
            {
                if (!value.IsInt())
                {
                    throw std::invalid_argument("invalid int type");
                }
                t = value.GetInt();
            }

            template< typename T >
            typename std::enable_if< std::is_integral< decay_t<T> >::value
                && !std::is_signed< decay_t<T> >::value
                && (sizeof(T) <= sizeof(int32_t))
                , void >::type
                operator () (const rapidjson::Value& value, T& t) const
            {
                if (!value.IsUint())
                {
                    throw std::invalid_argument("invalid uint type");
                }
                t = value.GetUint();
            }

            template< typename T >
            typename std::enable_if< std::is_integral< decay_t<T> >::value
                && std::is_signed< decay_t<T> >::value
                && (sizeof(T) > sizeof(int32_t) && sizeof(T) <= sizeof(int64_t))
                , void >::type
                operator () (const rapidjson::Value& value, T& t) const
            {
                if (!value.IsInt64())
                {
                    throw std::invalid_argument("invalid int64 type");
                }
                t = value.GetInt64();
            }

            template< typename T >
            typename std::enable_if< std::is_integral< decay_t<T> >::value
                && !std::is_signed< decay_t<T> >::value
                && (sizeof(T) > sizeof(int32_t) && sizeof(T) <= sizeof(int64_t))
                , void >::type
                operator () (const rapidjson::Value& value, T& t) const
            {
                if (!value.IsUint64())
                {
                    throw std::invalid_argument("invalid uint64 type");
                }
                t = value.GetUint64();
            }

            void operator () (const rapidjson::Value& value, bool& t) const
            {
                if (!value.IsBool())
                {
                    throw std::invalid_argument("invalid bool type");
                }
                t = value.GetBool();
            }

            template< typename T >
            typename std::enable_if< std::is_floating_point< decay_t<T> >::value
                , void >::type
                operator () (const rapidjson::Value& value, T& t) const
            {
                if (!value.IsDouble())
                {
                    throw std::invalid_argument("invalid double type");
                }
                t = value.GetDouble();
            }

            template< typename T >
            typename std::enable_if< isString< T >::value
                , void >::type
                operator () (const rapidjson::Value& value, T& t) const
            {
                if (!value.IsString())
                {
                    throw std::invalid_argument("invalid string type");
                }
                t = value.GetString();
            }

            template< typename T >
            typename std::enable_if< boost::fusion::traits::is_sequence< T >::type::value
                && !isSingleValueContainer< T >::value
                && !isMapContainer< T >::value
                , void >::type
                operator () (const rapidjson::Value& value, T& t) const
            {
                using Indices = boost::mpl::range_c< std::size_t, 0, boost::fusion::result_of::size< T >::value >;
                boost::fusion::for_each(Indices(), Visitor<T>(*this, value, t));
            }

            template< typename T >
            typename std::enable_if< isSingleValueContainer< T >::value
                , void >::type
                operator () (const rapidjson::Value& value, T& t) const
            {
                if (!value.IsArray())
                {
                    throw std::invalid_argument("invalid single value container type");
                }
                t.clear();
                auto size = value.Size();
                t.resize(size);
                for (rapidjson::SizeType i = 0; i < size; ++i)
                {
                    (*this)(value[i], t[i]);
                }
            }

            template< typename T >
            void operator () (const rapidjson::Value& value, std::set< T >& t) const
            {
                if (!value.IsArray())
                {
                    throw std::invalid_argument("invalid set type");
                }
                auto size = value.Size();
                for (rapidjson::SizeType i = 0; i < size; ++i)
                {
                    T temp;
                    (*this)(value[i], temp);
                    t.emplace(temp);
                }
            }

            template< typename T >
            void operator () (const rapidjson::Value& value, std::unordered_set< T >& t) const
            {
                if (!value.IsArray())
                {
                    throw std::invalid_argument("invalid unordered_set type");
                }
                auto size = value.Size();
                for (rapidjson::SizeType i = 0; i < size; ++i)
                {
                    T temp;
                    (*this)(value[i], temp);
                    t.emplace(temp);
                }
            }

            template< typename T >
            typename std::enable_if< isMapContainer< T >::value
                , void >::type
                operator () (const rapidjson::Value& value, T& t) const
            {
                if (!value.IsObject())
                {
                    throw std::invalid_argument("invalid map container type");
                }
                t.clear();
                for (auto itr = value.MemberBegin(), end = value.MemberEnd(); itr != end; ++itr)
                {
                    auto key = Boyee::fromString< typename T::key_type >(itr->name.GetString());
                    typename T::mapped_type temp;
                    (*this)(itr->value, temp);
                    t.emplace(key, temp);
                }
            }

            template< typename T, size_t N >
            void operator () (const rapidjson::Value& value, T(&t)[N]) const
            {
                if (!value.IsArray())
                {
                    throw std::invalid_argument("invalid array type");
                }
                auto size = value.Size();
                if (size != N)
                {
                    //throw std::invalid_argument("array size not match");
                    // TODO: 放宽限制，数组元素数量不一致，以最小值为准
                    size = size > N ? N : size;
                }
                for (rapidjson::SizeType i = 0; i < size; ++i)
                {
                    (*this)(value[i], t[i]);
                }
            }

            template< typename T, size_t N >
            void operator () (const rapidjson::Value& value, boost::array<T, N>& t) const
            {
                if (!value.IsArray())
                {
                    throw std::invalid_argument("invalid array type");
                }
                auto size = value.Size();
                if (size != N)
                {
                    //throw std::invalid_argument("array size not match");
                    // TODO: 放宽限制，数组元素数量不一致，以最小值为准
                    size = size > N ? N : size;
                }
                for (rapidjson::SizeType i = 0; i < size; ++i)
                {
                    (*this)(value[i], t[i]);
                }
            }

            template< typename T >
            void operator () (const char* szFieldName, const rapidjson::Value& value, T& t) const
            {
                if (!value.HasMember(szFieldName))
                {
                    std::string errorString = "[";
                    errorString += szFieldName;
                    errorString += "] not exist";
                    throw std::invalid_argument(errorString);
                }
                (*this)(value[szFieldName], t);
            }

            template< typename T >
            void operator () (const char* szFieldName, const rapidjson::Value& value, boost::optional< T >& t) const
            {
                if (value.HasMember(szFieldName))
                {
                    if (!t)
                    {
                        t.emplace();
                    }
                    (*this)(value[szFieldName], *t);
                }
                //         else
                //         {
                //             if (!t)
                //             {
                //                 t.reset();
                //             }
                //         }
            }

            template < typename Seq >
            struct Visitor : NonCopyable
            {
                const JsonDeserializer& deserializer;
                const rapidjson::Value& value;
                Seq& seq;

                Visitor(const JsonDeserializer& deserializer_, const rapidjson::Value& value_, Seq& seq_)
                    : deserializer(deserializer_)
                    , value(value_)
                    , seq(seq_)
                {
                }

                template< typename Index >
                void operator() (Index&& index) const
                {
                    const std::string& fieldName = boost::fusion::extension::struct_member_name< Seq, Index::value >::call();
                    deserializer(fieldName.c_str(), value, boost::fusion::at< Index >(seq));
                }
            };

        private:
            rapidjson::Document reader;
        };

        template< typename T >
        inline void fromJson(const char* json, T& t)
        {
            bzbee::Serialization::JsonDeserializer deserializer;
            deserializer.deserialize(json, t);
        }

        template< typename T >
        inline void fromJson(const std::string& json, T& t)
        {
            bzbee::Serialization::JsonDeserializer deserializer;
            deserializer.deserialize(json.c_str(), t);
        }

    } // !namespace Serialization
} // !namespace Boyee
