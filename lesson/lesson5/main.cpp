#include <iostream>
#include <array>
using namespace std;

#include <boost/mp11.hpp>

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/mpl.hpp>

template< class Seq >
constexpr std::array< const char*, boost::fusion::result_of::size< Seq >::value > get_member_names()
{
    std::array< const char*, boost::fusion::result_of::size< Seq >::value > members{};
    boost::mp11::mp_for_each< boost::mp11::mp_iota_c< boost::fusion::result_of::size< Seq >::value> >([&](auto I) {
        members[I] = boost::fusion::extension::struct_member_name< Seq, I >::call();
    });
    return members;
}

struct MyStruct
{
    int i;
    int32_t i32;
    uint32_t ui32;
    int64_t i64;
    uint64_t ui64;
    std::string str;
};

BOOST_FUSION_ADAPT_STRUCT(
    MyStruct
    , i32
    , ui32
    , i64
    , ui64
    , str
);

int main(int argc, char* argv[])
{
    MyStruct t;

    return 0;
}
