#include <iostream>
#include <string>
#include <functional>
#include <vector>
using namespace std;

#include "test.h"

void foo(test t)
{
    static test st;
    st = std::move(t);
}

/*
void foo(test&& t)
{
    static test st;
    st = t;
}*/

void bar(const std::function<void()>& fn)
{
    fn();
}

struct Base
{
    Base()
    {
        cout << "Base::Base()" << endl;
    }

    virtual ~Base()
    {
        cout << "Base::~Base()" << endl;
    }
};

struct A : public Base
{
    A()
        : Base()
    {
        cout << "A::A()" << endl;
    }

    ~A()
    {
        cout << "A::~A()" << endl;
    }
};

int main(int argc, char* argv[])
{
    // test t;

    // t.f1(1);
    // t.f1(0.1);

    // test t2("test");

    // foo(t);

    // bar([&t]() {
    // });

    // std::vector<test> vec;
    // vec.emplace_back(test(t));

    // A a1;
    // const Base& a2 = a1;
    // const A& a3 = a2;

    int size = 1200;
    int left = size;
    for (auto i = 0; i < size; ++i)
    {
        left--;
        if (i > 0 && i % 20 == 0)
        {
            cout << "i:" << i << endl;
        }
    }
    cout << "left:" << left << endl;

    return 0;
}
