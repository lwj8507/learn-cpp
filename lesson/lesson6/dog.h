#pragma once

#include <iostream>

#include "animal.h"

namespace zoo
{
    struct dog : public animal_crtp<dog>
    {
        E_Animal type() const override
        {
            return E_Animal_Dog;
        }

        int move() override
        {
            return 4;
        }

        void swim()
        {
            std::cout << "swim" << std::endl;
        }
    };
}; // !namespace zoo
