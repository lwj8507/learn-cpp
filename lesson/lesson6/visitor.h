#pragma once

namespace zoo
{
    struct visitor_token
    {
        virtual ~visitor_token() = default;
    };

    template<class T>
    struct visitor
    {
        virtual void visit(T*) const = 0;
        virtual void visit(T*, int) const = 0;
    };

    template<class... T>
    struct visitor_token_set : public visitor_token, public visitor<T>...
    {
        //using visitor<T>::visitor...;
    };
}; // !namespace zoo
