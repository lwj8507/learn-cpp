#include <iostream>
#include <memory>
using namespace std;

#include "visitor_impl.h"
using namespace zoo;

int main(int argc, char* argv[])
{
    auto d = std::make_unique<dog>();
    d->accept(visitor1::instance());
    d->accept(visitor1::instance(), 1);

    auto b = std::make_unique<bird>();
    b->accept(visitor1::instance());
    b->accept(visitor1::instance(), 2);

    auto f = std::make_unique<fish>();
    f->accept(visitor2::instance());
    f->accept(visitor2::instance(), 3);

    return 0;
}
