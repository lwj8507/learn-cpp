#pragma once

#include <iostream>

#include "animal.h"

namespace zoo
{
    struct bird : public animal_crtp<bird>
    {
        E_Animal type() const override
        {
            return E_Animal_Bird;
        }

        int move() override
        {
            return 2;
        }

        void fly()
        {
            std::cout << "fly" << std::endl;
        }
    };
}; // !namespace zoo
