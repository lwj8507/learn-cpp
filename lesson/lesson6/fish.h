#pragma once

#include <iostream>

#include "animal.h"

namespace zoo
{
    struct fish : public animal_crtp<fish>
    {
        E_Animal type() const override
        {
            return E_Animal_Fish;
        }

        int move() override
        {
            return 1;
        }

        void dive()
        {
            std::cout << "dive" << std::endl;
        }
    };
}; // !namespace zoo
