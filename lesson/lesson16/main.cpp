﻿#include <uvw.hpp>

int main(int argc, char** argv)
{
    auto loop = uvw::Loop::getDefault();
    auto has_run = std::make_shared<bool>();
    auto cb = [](std::shared_ptr<void> data) {
        if (auto has_run = std::static_pointer_cast<bool>(data); has_run) {
            *has_run = true;
        }
    };

    auto handle = loop->resource<uvw::Thread>(cb, has_run);

    loop->run();
    return 0;
}
