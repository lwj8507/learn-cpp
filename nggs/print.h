#pragma once

#include <iostream>
#include <sstream>

namespace nggs
{
    template <typename Arg>
    void PrintOne(std::ostream &out, const Arg &arg)
    {
        out << arg;
    }

#if __cplusplus >= 201703L

    template <typename... Args>
    void Println(const Args &...args)
    {
        std::ostringstream buffer;
        (PrintOne(buffer, args), ...);
        buffer << std::endl;
        std::cout << buffer.str() << std::flush;
    }

#elif __cplusplus >= 201103L || _MSC_VER == 1800

    template <typename... Args>
    void Println(const Args &...args)
    {
        std::ostringstream buffer;
        std::initializer_list<int>{(PrintOne(buffer, args), 0)...};
        buffer << std::endl;
        std::cout << buffer.str() << std::flush;
    }

    // void println(std::ostream& out)
    //{
    //     out << std::endl;
    // }
    //
    // template < typename Head, typename... Rests >
    // void println(std::ostream& out, const Head& head, const Rests&... rests)
    //{
    //     PrintOne(out, head);
    //     println(out, rests...);
    // }
    //
    // template < typename... Args >
    // void Println(const Args&... args)
    //{
    //     std::ostringstream buffer;
    //     println(buffer, args...);
    //     std::cout << buffer.str() << std::flush;
    // }

#else

    static_assert(true, "please use c++11 or higher");

#endif
} // !namespace nggs
