
namespace nggs
{
#if __cplusplus >= 201703L

    template <typename Key, typename... Args>
    bool AnyOne(Key &&key, Args &&...args)
    {
        return ((args == key) || ...);
    };

    template <typename Key, typename... Args>
    bool All(Key &&key, Args &&...args)
    {
        return ((args == key) && ...);
    };

#else

    static_assert(true, "please use c++17 or higher");

#endif
} // !namespace nggs
