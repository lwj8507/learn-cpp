WORK_DIR=$(shell pwd)
################################################################################################
.PHONY: all clean cmake cmaked \
lesson1 lesson2 lesson3 lesson4 lesson5 lesson6 lesson7 lesson8 lesson9 lesson10 \
lesson11 lesson12 lesson13 lesson14 lesson15 \
test1 test2 test3 test4 test5 test6 test7 test8 test9 test10 \
benchmark1
################################################################################################
all:
	cd ${WORK_DIR}/build;cmake --build .
################################################################################################
clean:
	rm -rf ${WORK_DIR}/build
################################################################################################
cmake:clean
	mkdir ${WORK_DIR}/build;cd ${WORK_DIR}/build;cmake -DCMAKE_BUILD_TYPE:STRING=Release -B . -S ..

cmaked:clean
	mkdir ${WORK_DIR}/build;cd ${WORK_DIR}/build;cmake -DCMAKE_BUILD_TYPE:STRING=Debug -B . -S ..
################################################################################################
lesson1:
	cd ${WORK_DIR}/build;make lesson1

lesson2:
	cd ${WORK_DIR}/build;make lesson2

lesson3:
	cd ${WORK_DIR}/build;make lesson3

lesson4:
	cd ${WORK_DIR}/build;make lesson4

lesson5:
	cd ${WORK_DIR}/build;make lesson5

lesson6:
	cd ${WORK_DIR}/build;make lesson6

lesson7:
	cd ${WORK_DIR}/build;make lesson7

lesson8:
	cd ${WORK_DIR}/build;make lesson8

lesson9:
	cd ${WORK_DIR}/build;make lesson9

lesson10:
	cd ${WORK_DIR}/build;make lesson10

lesson11:
	cd ${WORK_DIR}/build;make lesson11

lesson12:
	cd ${WORK_DIR}/build;make lesson12

lesson13:
	cd ${WORK_DIR}/build;make lesson13

lesson14:
	cd ${WORK_DIR}/build;make lesson14

lesson15:
	cd ${WORK_DIR}/build;make lesson15
################################################################################################
test1:
	cd ${WORK_DIR}/build;make test1

test2:
	cd ${WORK_DIR}/build;make test2

test3:
	cd ${WORK_DIR}/build;make test3

test4:
	cd ${WORK_DIR}/build;make test4

test5:
	cd ${WORK_DIR}/build;make test5

test6:
	cd ${WORK_DIR}/build;make test6

test7:
	cd ${WORK_DIR}/build;make test7

test8:
	cd ${WORK_DIR}/build;make test8

test9:
	cd ${WORK_DIR}/build;make test9

test10:
	cd ${WORK_DIR}/build;make test10
################################################################################################
benchmark1:
	cd ${WORK_DIR}/build;make benchmark1
